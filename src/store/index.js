import { createStore } from 'vuex'
import { addUnit, addChapter, editChapter, deleteUnit, deleteChapter } from '../firebase.js'

export default createStore({
  state: {
    user: null,
    username: null,
    isEditing: false,
    graphItems: [
      {
        date: new Date("2021/10/1"),
        task: 'פרקים 1-3'
      },
    ],
    fetchedUnits: false,
    units: {},
    fetchedPodcasts: false,
    podcasts: {}
  },
  actions: {
      addUnit(store, title) {
        addUnit(title)
      },
      addChapter(store, {unitid, data}) {
        addChapter(unitid, data)
      },
      removeUnit(store, {unitid}) {
        deleteUnit(unitid)
      },
      removeChapter(store, {unitid, chapterid}) {
        deleteChapter(unitid, chapterid)
      },
      editChapter(store, {unitid, data}) {
        editChapter(unitid, data)
      },
    initChapterStatuses() {
      if (localStorage.getItem('chaptersStatuses')) {
        let arr = JSON.parse(localStorage.getItem('chaptersStatuses'))
        for (let index in arr) {
          if (arr[index]) {
            this.dispatch('changeChapterStatus', {id: index, status: arr[index]})
          }
        }
      } else {
        let obj = JSON.stringify({})
        localStorage.setItem('chaptersStatuses', obj)
      }
    },
    changeChapterStatus({commit, dispatch, state}, {id, status}) {
      for (let unit in state.units) {
        if (state.units[unit].chapters) {
          let chaptersArr = Object.values(state.units[unit].chapters)
          let doneChapter = chaptersArr.find(chapter => chapter.id == id)
          if (doneChapter != undefined) {
            commit('setStatusInState', {chapter: doneChapter, status: status})
            dispatch('saveChapterStatus', {id: doneChapter.id, status: status})
            return
          }
        }
      }
    },
    saveChapterStatus(state, {id, status}) {
      let obj = JSON.parse(localStorage.getItem('chaptersStatuses'))
      obj[id] = status
      obj = JSON.stringify(obj)
      localStorage.setItem('chaptersStatuses', obj)
    },
  },
  mutations: {
    setStatusInState(state, {chapter, status}) {
      chapter.done = status
    }
  },
  getters: {
    getChapterById: (state) => (id) => {
      for (let unit in state.units) {
        let chaptersArr = Object.values(state.units[unit].chapters)
        let chapter = chaptersArr.find(chapter => chapter.id == id)
        if (chapter != undefined) {
          return chapter
        }
      }
    }
  },
  modules: {
  }
})
