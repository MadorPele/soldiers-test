import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Units from '../views/units-page.vue'
import Podcasts from '../views/Podcasts-page.vue'

const routes = [
  {
    path: '/',
    redirect: '/home'
  }, 
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/units',
    name: 'units',
    component: Units
  },
  {
    path: '/podcasts',
    name: 'podcasts',
    component: Podcasts
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
