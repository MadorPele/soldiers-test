<div align="center">

# אתר המבחן החיילי

</div>
<div align="center">

# [View Online](https://madorpele.gitlab.io/soldiers-test)

</div>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
